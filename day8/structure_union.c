
#include <stdio.h>
struct Scope {

	union {
		char alpha;
		int num;
	};
};

int main()
{
	struct Scope x, y;
	x.num = 65;
	y.alpha = 'h';

	printf("y.alpha = %c, x.num = %d", y.alpha, x.num);

	return 0;
}
