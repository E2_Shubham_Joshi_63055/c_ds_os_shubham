#include<stdio.h>
#include<string.h>

void mymemcpy(void *dest,void *src,size_t n)
{
    char *csrc=(char *)src;
    char *cdest=(char *)dest;

    for(int i=0;i<n;i++)
    {
    cdest[i]=csrc[i];
    }
}
int main()
{
    char str[10]="shubham";
    char str2[10]="joshi";
    
    printf("str2 before :");
    printf("%s",str2);
    mymemcpy(str2,str,strlen(str)+1);
    //memcpy(str,str2,sizeof(str2));

    printf("string after :\n");
    printf("%s",str2);

}