#include<stdio.h>
#include<string.h>
#include<stdlib.h>


void mymemmove(void *dest,void *src,int n)
{
    char *csrc=(char *)src;
    char *cdest=(char *)dest;

    char *temp=(char *)malloc(sizeof(char)*n);
    if(NULL==temp)
    {
        return NULL;
    }
    else
    {
        unsigned int i=0;
        for(i=0;i<n;++i)
        {
            *(temp+i)=*(csrc+i);
        
        }
        for(i=0;i<n;++i)
        {
            *(cdest+i)=*(temp+i);
        }
        free(temp);
    }
}
int main()
{
    char s1[20]="shubham";
   // char s2[20]="mumbai";

    printf("before %s\n",s1);
   // memmove(s1,s2,strlen(s2)+1);
   mymemmove(s1+5,s1,strlen(s1)+1);

    printf("after %s\n",s1);
}