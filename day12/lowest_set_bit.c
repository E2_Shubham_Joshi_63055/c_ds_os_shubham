#include <stdio.h>


int main()
{
    int num, order, i;

  
    printf("Enter any number: ");
    scanf("%d", &num);

  

    /* Iterate through each bit of integer */
    for(i=0; i<32; i++)
    {
        /* If current bit is set */
        if((num>>i) & 1)
        {
            order = i;

            /* Terminate the loop */
            break;
        }
    }

    printf("Lowest order set bit in %d is %d", num, order);

    return 0;
}