#include<stdio.h>

#define printdebug(x) printf("value of %s is %d\n",#x,x)

int main()
{
    int value1,value2;
    
    value1=10;
    value2=20;
    
    printdebug(value1);
    printdebug(value2);
}
