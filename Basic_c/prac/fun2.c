#include<stdio.h>
void add (int a,int b)
{
    printf("%d\n",a+b);
}
void sub (int a,int b)
{
    printf("%d\n",a-b);
}
void mul (int a,int b)
{
    printf("%d\n",a*b);
}
void divi (int a,int b)
{
    printf("%d\n",a/b);
}

void display(void (*fptr)(int,int))
{
    (*fptr)(7,2);
}
int main()
{
    display(add);
    display(sub);
    display(mul);
    display(divi);
}