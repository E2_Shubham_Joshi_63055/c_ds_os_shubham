#include<stdio.h>

struct ab
{
    float v;
};

int main()
{  
    struct ab s1,s2,s3;
    s1.v=4.5;
    s2.v=8.9;
    //s3=s1+s2;//not right
    s3.v=s1.v+s2.v;  //right

    printf("%f",s3.v);

}
