#include<stdio.h>

void my_function()
{
    printf("This is my function");
}
void my_callback_function(void(*fptr)())
{

    printf("This is callback function");
    (*fptr)();
}
int main()
{
    void (*fptr)()=my_function;
    my_callback_function(fptr);
}