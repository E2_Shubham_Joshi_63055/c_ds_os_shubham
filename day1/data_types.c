#include<stdio.h>
#include<limits.h>

int main(void)
{
    printf("-------------------------------------------------------\n");
    printf("%-20s %-20s %-5s %-10s %-10s\n", "Data type", "Format Specifier", "Size", "Lower Range", "Upper Range");
    printf("-------------------------------------------------------\n");
    printf("%-20s %-20s %-5d %-10d %-10d\n", "signed char", "%c", sizeof(char), CHAR_MIN, CHAR_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "unsigned char", "%c", sizeof(unsigned char), 0, UCHAR_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", " int ", "%d", sizeof(int), INT_MIN, INT_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "unsigned short int", "%hu", sizeof(int), 0, USHRT_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", " short int", "%hd", sizeof(int), SHRT_MIN,SHRT_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "long int ", "%ld", sizeof(int),   INT_MIN, INT_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "unsigned long int", "%lu", sizeof(int), 0, ULONG_MAX);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "float", "%f", sizeof(float),__FLT_MIN_10_EXP__,__FLT_MAX_10_EXP__);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "double", "%lf", sizeof(double), __DBL_MIN__, __DBL_MAX__);
    printf("%-20s %-20s %-5d %-10d %-10d\n", "long double", "%Lf", sizeof(double), __LDBL_MIN__, __LDBL_MAX__);


    printf("-------------------------------------------------------\n");
    return 0;
}