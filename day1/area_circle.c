#include<stdio.h>

int main() {

   int radius;
   float PI = 3.14, area, circumference;

   printf("Enter radius of circle: \n");
   scanf("%d", &radius);


//area of circle = pi*r*r
   area = PI * radius * radius;
   printf("Area of circle : %f \n", area);
//circumference of circle = 2*pi*r
   circumference = 2 * PI * radius;
   printf("Circumference : %f \n", circumference);

   return (0);
}