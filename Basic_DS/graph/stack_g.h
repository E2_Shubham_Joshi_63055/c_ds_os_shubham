#ifndef __STACK_H
#define __STACK_H

#define SIZE 64
//type declaration for stack
typedef struct stack
{
    int arr[SIZE];
    int top;
}stack_t;

void s_init(stack_t *st);
void s_push(stack_t *st,int data);
void s_pop(stack_t *st);
int s_peek(stack_t *st);
int s_is_empty(stack_t *st);
int s_is_full(stack_t *st);

#endif   