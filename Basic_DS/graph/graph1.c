#include<stdio.h>
#include"stack.h"
#include"queue1.h"

#define MAX 10

typedef struct graph
{
    int mat[MAX][MAX];
    int vet_cnt;
}graph_t;

graph_t g1;

void init_graph(void);
void accept_graph(void);
void print_graph(void);

void dfs_traversal(int start);
void bfs_traversal(int start);

int main(void)
{
    init_graph();
    accept_graph();
    print_graph();

    printf("DFS Traversal :: ");
    dfs_traversal(0);
    printf("\n");

    printf("BFS Traversal :: ");
    bfs_traversal(0);
    printf("\n");
    return 0;
}

void init_graph(void)
{
    printf("Enter vertex count : ");
    scanf("%d", &g1.vet_cnt);
    for(int i = 0 ; i < g1.vet_cnt ; i++)
    {
        for(int j = 0 ; j < g1.vet_cnt ; j++)
            g1.mat[i][j] = 0;
    }
}

void accept_graph(void)
{
    printf("Enter %d X %d matrix : ", g1.vet_cnt, g1.vet_cnt);
    for(int i = 0 ; i < g1.vet_cnt ; i++)
    {
        for(int j = 0 ; j < g1.vet_cnt ; j++)
            scanf("%d", &g1.mat[i][j]);
    }
}

void print_graph(void)
{
    printf("Graph is :: \n");
    for(int i = 0 ; i < g1.vet_cnt ; i++)
    {
        for(int j = 0 ; j < g1.vet_cnt ; j++)
            printf("%-4d", g1.mat[i][j]);
        printf("\n");
    }
}

void dfs_traversal(int start)
{
    stack_t st;
    int visited[MAX] = {0};
    s_init(&st);
    // push start vertex on stack and mark it as visited
    s_push(&st, start);
    visited[start] = 1;
    // repeat till stack is not empty
    while (!s_is_empty(&st))
    {
        // pop one vertex from stack
        int trav = s_peek(&st); s_pop(&st);
        // visit poped vertex
        printf("%-4d", trav);
        // push all non visited vertices on stack
        for(int j = 0 ; j < g1.vet_cnt ; j++)
        {
            if(g1.mat[trav][j] == 1 && visited[j] == 0)
            {
                s_push(&st, j);
                visited[j] = 1;
            }
        }
    }
    


}
void bfs_traversal(int start)
{
    que_t q;
    int visited[MAX] = {0};
    q_init(&q);
    // push start vertex on queue and mark it as visited
    q_enqueue(&q, start);
    visited[start] = 1;
    // repeat till queue is not empty
    while (!q_is_empty(&q))
    {
        // pop one vertex from queue
        int trav = q_peek(&q); q_dequeue(&q);
        // visit poped vertex
        printf("%-4d", trav);
        // push all non visited vertices on queue
        for(int j = 0 ; j < g1.vet_cnt ; j++)
        {
            if(g1.mat[trav][j] == 1 && visited[j] == 0)
            {
                q_enqueue(&q, j);
                visited[j] = 1;
            }
        }
    }
}

/*
0 1 1 1 0 0
1 0 1 0 1 0
1 1 0 0 0 0
1 0 0 0 1 1
0 1 0 1 0 0
0 0 0 1 0 0
*/