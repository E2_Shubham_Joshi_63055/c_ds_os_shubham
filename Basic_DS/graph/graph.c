#include<stdio.h>
#include"stack_g.h"
#include"queue_g.h"

#define SIZE 64

typedef struct graph
{
    int max[SIZE][SIZE];
    int vert_cnt;
}graph_t;

graph_t g1;

void init_graph(void);
void accept_graph(void);
void print_graph(void);

void dfs_traversal(int start);
void bfs_traversal(int start);

int main()
{
    init_graph();
    accept_graph();
    print_graph();

    printf("DFS Traversal :: ");
    dfs_traversal(0);
    printf("\n");

    printf("BFS Traversal :: ");
    bfs_traversal(0);
    printf("\n");
    return 0;
}
void init_graph(void)
{
    printf("Enter vertex count : ");
    scanf("%d",&g1.vert_cnt);
    for(int i=0;i<g1.vert_cnt;i++)
    {
        for(int j=0;j<g1.vert_cnt;j++)
        g1.max[i][j]=0;
    }
}


void accept_graph(void)
{
    printf("Enter %d x %d matrix : ",g1.vert_cnt,g1.vert_cnt);
    for(int i=0;i<g1.vert_cnt;i++)
    {
        for(int j=0;j<g1.vert_cnt;j++)
        scanf("%d",&g1.max[i][j]);
        printf("\n");
    }
}

void print_graph(void)
{
    printf("Graph is :: \n");
    for(int i=0;i<g1.vert_cnt;i++)
    {
        for(int j=0;j<g1.vert_cnt;j++)
        printf("%-4d",g1.max[i][j]);
        printf("\n");
    }
}

void dfs_traversal(int start)
{
    stack_t st;
    int visited [SIZE] ={0};
    s_init(&st);
    //push start vertex on stack as visited
    s_push(&st,start);
    visited[start]=1;
    //repeat till stack is not empty
    while (!s_is_empty(&st))
    {
        //pop one vertex from stack
        int trav =s_peek(&st);s_pop(&st);
        //visit popped vertex
        printf("%-4d",trav);
        //push all non visited vertices o stack
        for(int j=0;j<g1.vert_cnt;j++)
        {
            if(g1.max[trav][j] ==1 && visited[j] ==0)
            {
                s_push(&st,j);
                visited[j] =1;
            }
        }


    }

    
    
}

void bfs_traversal(int start)
{
    que_t q;
    int visited[SIZE] = {0};
    q_init(&q);
    //push start vertex on queue and mark visited
    q_enqueue(&q,start);
    visited[start] =1;
    //repear till queue is not empty
    while(!q_is_empty(&q))
    {
        //pop one vertex from queue
        int trav =q_peek(&q);q_dequeue(&q);
        //visit poped vertex
        printf("%-4d",trav);
        //push all non visited vertices on queue
        for(int j=0;j<g1.vert_cnt;j++)
        {
            if(g1.max[trav][j] == 1 && visited[j] == 0)
            {
                q_enqueue(&q,j);
                visited[j]=1;
            }
        }

    }
}