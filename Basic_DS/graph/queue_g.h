#ifndef __QUEUE_H
#define __QUEUE_H


#define SIZE 64
//declare queue type
typedef struct queue
{
    int arr[SIZE];
    int front ,rear;
}que_t;

void q_init(que_t *q);
void q_enqueue(que_t *q,int data);
void q_dequeue(que_t *q);
int q_peek(que_t *q);
int q_is_empty(que_t *q);
int q_is_full(que_t *q);

#endif