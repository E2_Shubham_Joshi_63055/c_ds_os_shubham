#include<stdio.h>
#define N 5

int queue[N];  //declare global as use in in various global
int front=-1;
int rear = -1;

void enqueue()
{
    int t;
    printf("Enter data:");
    scanf("%d",&t);
    if(rear==N-1)
    {
        printf("Overflow");
    }
    else if (front ==-1 && rear ==-1)
    {
        front=rear=0;
        queue[rear]=t;
    }
    else
    {
        rear++;
        queue[rear]=t;
    }
    
}

void dequeue()
{
    if(front == -1 && rear == -1)
    {
        printf("Underflow");
    }
    else if(front == rear)
    {
        front=rear=-1;
    }
    else
    {   
        printf("Dequeue element is %d",queue[front]);
    
        front++;

        printf("Dequeue element AFTER is %d",queue[front]);
    }
}

void display()
{
    int i;
    if(front == -1 && rear == -1)
    {
        printf("Queue is empty");
    }
    else
    {
        for(i=front;i<rear+1;i++)
        {
            printf("%d \n",queue[i]);
        }
    }
}

void peek ()
{
    if(front == -1 && rear ==-1)
    printf("Queue is empty");
    else 
    printf("%d ",queue[front]);
}
int main()
{
    int ch;
   
    do{
        printf("Enter choice \n1:Enqueue \n2:Dequeue \n3:Peek \n4:Display :\n");
        scanf("%d",&ch);
        switch(ch)
        {
            case 1: enqueue();
                    break;
            case 2: dequeue();
                    break;
            case 3: peek();
                    break;
            case 4: display();
                    break;
            default: printf("Invalid choice");
        }
    }while (ch!=0);
    
}