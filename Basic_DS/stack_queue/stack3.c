#include<stdio.h>
#define N 5

typedef  struct lifo{
    int st[N];
    int top;
} stack;

void init(stack *s)
{
    s->top=-1;
}

void push(stack *s)

{
    int x;
    printf("Enter element :\n");
    scanf("%d",&x);
    if (s->top==N-1)
    printf("stack is full\n");
    else
    s->top++;
    s->st[s->top]=x;
}

void pop(stack *s)
{
    if(s->top==N-1)
    printf("Underflow\n");
    else
    s->st[s->top--];
}

void display(stack *s)
{
    //if(s->top==N-1)
    //printf("Stack is empty\n");
    
    for(int i=s->top;i>=0;i--)
    {
    printf("%d\n",s->st[i]);
    }

}
void go_back(stack *s)    
 {
    pop(s);
}

   
   
int main ()     {
    stack s1;
    s1.top=-1;
    push(&s1); //1
    push(&s1);  //2 1
    push(&s1);  // 3 2 1
    go_back(&s1);  //2 1
    go_back(&s1); // 1
    display(&s1);  //1
    push(&s1);   // 4 1
    display(&s1);  // 4 1 

    go_back(&s1);  // 1
    display(&s1);  //1
    return 0;
}