#include<stdio.h>
#include<stdlib.h>

#define SIZE 9

void merge_sort(int *arr,int left,int right);
void print_array(int *arr,int size);

int main()
{
    int arr[SIZE]={6,3,7,9,1,4,5,8,2};

    print_array(arr,SIZE);
    merge_sort(arr,0,SIZE-1);
    print_array(arr,SIZE);

    return 0;


}

void merge_sort(int *arr,int left,int right)
{
    //if single or no element in partition
    if(left>=right)
    return;
    //divide array into two parts
    int mid=(left+right)/2;
    //sort individual partition by applying same algoritm
    merge_sort(arr,left,mid);
    merge_sort(arr,mid+1,right);
    //merge two sorted partiton into one of temp array
    int n=right-left+1;
    int *temp=(int*)malloc(n*sizeof(int));
    int i=left,j=mid+1,k=0;
    while( i<= mid && j<= right)
    temp[k++] = arr[i] < arr[j] ? arr[i++] : arr[j++];

    while( i <= mid)
    temp[k++]=arr[i++];
    

    while (j<=right)
    temp[k++]=arr[j++];
   
   //overwrite temp array into orignal
   for(i=0;i<n;i++)
   arr[left+i]= temp[i];

   free(temp);
    }

    void print_array(int *arr,int size)
    {
        printf("Array:");
        for(int i=0;i<size;i++)
        printf("%-4d",arr[i]);
    }