#include<stdio.h>

#define SIZE 6
#define SWAP(n1,n2,type) {type t =n1;n1=n2;n2=t;}

void print_array(int *arr,int size);
void selection_sort(int *arr,int size);

int main()
{
    int arr[SIZE]={33,44,11,88,55,22};

    print_array(arr,SIZE);
    selection_sort(arr,SIZE);
    print_array(arr,SIZE);

    return 0;
}

void print_array(int *arr,int size)
{
    printf("Array:");
    for(int i=0;i<size;i++)
    {
        printf("%-4d",arr[i]);

        printf("\n");
    }
}

void selection_sort(int *arr,int size)
{
    int pass =0;
    int comp =0;
    for(int i=0;i<size-1;i++)
    {
        pass++;
        for(int j=i+1;j<size;j++)
        {
            comp++;
            if(arr[i] > arr[j])
            SWAP(arr[i],arr[j],int);
        }
    }
    printf("pass=%d\n camparision=%d\n",pass,comp);
}