#include<stdio.h>
#define SIZE 10
#define SWAP(n1,n2,type) {type t=n1;n1=n2;n2=t;}


void print_array(int*arr,int size);
void insertion_sort(int *arr,int size);

int main()
{
    int arr[SIZE]={44,22,66,88,11,99,33};

    print_array(arr,SIZE);
    insertion_sort(arr,SIZE);
    print_array(arr,SIZE);

    return 0;
}

void print_array(int *arr,int size)
{
    printf("Array:");
    for(int i=0;i<size;i++)
    printf("%-4d",arr[i]);
}

void insertion_sort(int *arr,int size)
{
    int pass =0;
    for(int i=1;i<size;i++)
    {
        pass++;

        int temp=arr[i];
        int j;
        for(j=i-1;j>=0 && arr[j]>temp;j--)
            arr[j+1]=arr[j];
            arr[j+1]=temp;
    }
    printf("pass=%d  ",pass);
}