#include<stdio.h>

#define SIZE 6

#define SWAP(n1,n2,type) {type t =n1;n1=n2;n2=t;}

void print_array(int *arr,int size);
void bubble_sort(int *arr,int size);

int main()
{
    int arr[SIZE]={22,99,44,11,88,33};
    print_array(arr,SIZE);
    bubble_sort(arr,SIZE);
    print_array(arr,SIZE);

    return 0;

}

void print_array(int *arr,int size)
{
    printf("Array:");
    for(int i=0;i<size;i++)
    {
        printf("%-4d",arr[i]);
    }
}

void bubble_sort(int *arr,int size)
{
    int pass=0,comp=0;
    for(int i=0;i<size-1;i++)
    {
        pass++;
        for(int j=0;j<size-1;j++)
        {
            comp++;
            if(arr[j]>arr[j+1])
            {
                SWAP(arr[j],arr[j+1],int)
            }
        }
    }
    printf("\npass=%d  comp=%d",pass,comp);
}