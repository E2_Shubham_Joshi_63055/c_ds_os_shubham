#include<stdio.h>
#include<stdlib.h>

#define SIZE 9

#define SWAP(n1,n2,type) {type t=n1;n1=n2;n2=t;}

void quick_sort(int *arr,int left,int right);
void print_array(int *arr,int size);

int main()
{
    int arr[SIZE] = {66,88,33,55,11,77,44,22,99};
    print_array(arr,SIZE);
    quick_sort(arr,0,SIZE-1);
    print_array(arr,SIZE);

    return 0;

}

void quick_sort(int *arr,int left,int right)
{
    int i,j;
    //stop when invalid and sorted partition(single partition)
    if(left >= right)
    return;
    //select pivot element
    //arr[left]
    i=left;j=right;

    while(i<j)
    {
        //find greater element than pivot from left side
        for (;i <= right && arr[i] <= arr[left];i++);

        //find smaller and equal element than pivot from right side

        for(;j >= left && arr[j] > arr[left] ;j--);

        //swap both elements if i and j are not crossed
        if(i<j)
            SWAP( arr[i],arr[j],int);
        
    } //repeat till i and j not crossed

    //swap jth element and pivot
    SWAP(arr[j],arr[left],int);
    //sort left partition
    quick_sort(arr,left,j-1);
    //sort right partition
    quick_sort(arr,j+1,right);
}

void print_array(int *arr,int size)
{
    printf("Array:");
    for(int i=0;i<size;i++)
        printf("%-4d",arr[i]);
}