/*linked list with 5 node and char data vowel*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
    int data;
    struct node *next;
}node_t;

typedef struct list
{
node_t *head;
node_t *tail;
int count;
}list_t;

void init_list(list_t *list);
int is_empty(list_t *list);
node_t *create_node(char data);

void add_node_first(list_t *list,char data);
void add_node_last(list_t *list,char data);
void add_node_position(list_t *list,char data,int pos);

void delete_node_first(list_t *list);
void delete_node_last(list_t *list);
void delete_node_position(list_t *list,int pos);

void display_list(list_t *list);
void free_list(list_t *list);

int main()
{
    list_t l1;
    init_list(&l1);
    l1.count=0;
        add_node_first(&l1,'o');
    add_node_first(&l1,'i');
    add_node_first(&l1,'e');
    add_node_first(&l1,'a');
    add_node_position(&l1,'o',4);
    add_node_last(&l1,'u');

  //  delete_node_last(&l1);
  //  delete_node_position(&l1,2);


    display_list(&l1);
    printf("no.pf nodes =%d\n",l1.count);
    free_list(&l1);
    
}

void init_list(list_t *list)
{
    list->head =NULL;
    list->tail=NULL;
}

int is_empty(list_t *list)
{
    return list->head == NULL && list->tail == NULL;
}

node_t *create_node(char data)
{
    node_t *newnode=(node_t*)malloc(sizeof(node_t));
    newnode->data=data;
    newnode->next=NULL;

    return newnode;
}

void add_node_first(list_t *list,char data)
{
    node_t *newnode=create_node(data);
    if(is_empty(list))
    {
    list->head=list->tail=newnode;
    newnode->next=list->head;
    }
    else{
        newnode->next=list->head;
        list->head=newnode;
        list->tail->next=list->head;
    }
    list->count++;
}

void add_node_last(list_t *list,char data)
{
    node_t *newnode=create_node(data);

    if(is_empty(list))
    {
  list->head=list->tail=newnode;
      newnode->next=list->head;
    }
    else
    {
        newnode->next=list->head;
        list->tail->next=newnode;
        list->tail=newnode;
    }
    list->count++;
}
void add_node_position(list_t *list,char data,int pos)
{
    node_t *newnode=create_node(data);
    if(is_empty(list))
    {
    list->head=list->tail=newnode;
      newnode->next=list->head;
    }
    else if(pos==1)
    {
    free(newnode);
    add_node_first(list,data);
    return;
    }
    else
    {
        node_t *trav=list->head;
        for(int i=1; i<pos-1;i++)
        trav=trav->next;
        newnode->next=trav->next;
        trav->next=newnode;
        
    }
    list->count++;
}

void delete_node_first(list_t *list)
{
    if(is_empty(list))
    printf("list is empty");
    else if(list->head->next == list->head)
    {
        free(list->head);
        list->head=list->tail=NULL;
    }
    else
    {
        node_t *temp=list->head;
        list->head=temp->next;
        list->tail->next=list->head;
        free(temp);
    }
}

void delete_node_last(list_t *list)
{
    if(is_empty(list))
    printf("list is empty");
    else if(list->head->next == list->head)
    {
        free(list->head);
        list->head=list->tail=NULL;
    }
    else{
        node_t *trav=list->head;
       do
       {
        trav=trav->next;
       } while (trav->next->next != list->head);
       free(list->tail);
       list->tail=trav;
       list->tail->next=list->head;

    }
}

void delete_node_position(list_t *list,int pos)
{
    if(is_empty(list))
    printf("list is empty");
    else if(pos ==1)
    {
        delete_node_first(list);
        return;
    }
   
    else{
        node_t *trav=list->head;
        for(int i=1;i<pos-1;i++)
        trav=trav->next;

        node_t *temp=trav->next;
        trav->next=temp->next;
        free(temp);
    }
}
void display_list(list_t *list)
{
    if(is_empty(list))
    printf("list is empty");
    else {
          printf("List :");
        node_t *trav=list->head;

      
      do
        {
            printf("%-4c",trav->data);
            trav=trav->next;
        }while (trav != list->head);
    }
}

void free_list( list_t *list)
{
    if(is_empty(list))
    printf("List is empty");
    else{
        node_t *trav=list->head;
        do
        {
            node_t *temp=trav;
            trav=trav->next;
            free(temp);
        }while(trav!= list->head);
        list->head=list->tail=NULL;
    }
}