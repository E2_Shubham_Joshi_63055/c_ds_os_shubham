//c programme to remove alternate node of linked list

#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

void display(struct node* node)
{
    while(node!=NULL)
    {
        printf("%d ",node->data);
        node=node->next;
    }
    printf("\n");
}

void delete_alt(struct node *head)
{
    if(head == NULL)
    return;

    struct node *prev=head;
    struct node *curr=head->next;

    while(prev != NULL && curr != NULL)
    {
        prev->next = curr->next;

        free(curr);

        prev=prev->next;
        if(prev != NULL)
        curr = prev->next;
    }
}

int main()
{
    struct node* head=NULL;
    struct node* node2=NULL;
    struct node* node3=NULL;
    struct node* node4=NULL;
    struct node* node5=NULL;


    head=(struct node*)malloc(sizeof(struct node));
    node2=(struct node*)malloc(sizeof(struct node));
    node3=(struct node*)malloc(sizeof(struct node));
    node4=(struct node*)malloc(sizeof(struct node));
    node5=(struct node*)malloc(sizeof(struct node));

    head->data=5;
    head->next=node2;

    node2->data=4;
    node2->next=node3;

    node3->data=3;
    node3->next=node4;

    node4->data=2;
    node4->next=node5;

    node5->data=1;
    node5->next=NULL;

    printf("Before deletion:");

    display(head);

    delete_alt(head);

    printf("After deletion:");
    display(head);
}