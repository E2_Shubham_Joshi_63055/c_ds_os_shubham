//queue implement using linked list

#include<stdio.h>
#include<stdlib.h>

 struct node {
    int data;
    struct node *next;

};
struct node *front =NULL;
struct node *rear=NULL;

void enqueue(int data)
{
    struct node *ptr;
    ptr=(struct node*)malloc(sizeof(struct node));
    ptr->data=data;
    ptr->next=NULL;

    if((front == NULL) &&(rear == NULL))
    {
    front=rear=ptr;
    }
else{
    rear->next=ptr;
    rear=ptr;
}
  printf("Node is inserted\n");
    
}

int dequeue()
{
    if(front == NULL)
    {
        printf("\nUnderflow\n");
        return -1;
    }
    else{
        struct node *temp=front;
        int temp_data=front->data;
        front=front->next;
        free(temp);
        return temp_data;
       
        
    }
}

void display()
{
    struct node *temp;
    if((front ==NULL && rear == NULL))
    {
        printf("Queue is empty\n");

    }
    else
    {
        printf("The queue is \n");
        temp=front;
        while(temp)
        {
            printf("%d--->",temp->data);
            temp=temp->next;
        }
        printf("NULL\n");
    }
}

int main()
{
    int choice,data;
    printf("Implementation of queue using linked list \n");
    while(choice !=4)
    {
        printf("1.Enqueue\n2.Dequeue\n3.Display\n4.Exit\n");
        printf("Enter your choice :");
        scanf("%d",&choice);

        switch (choice)
        {
            case 1:
                 printf("\nEnter value to insert:");
                 scanf("%d",&data);
                 enqueue(data);
                 break;
            case 2:
                  printf("popped element is : %d\n ",dequeue());
                  break;
            case 3:
                display();
                break;
            case 4:
                 exit(0);
                 break;
            default:
                    printf("Wrong choice");
        }
    }
    return 0;
}