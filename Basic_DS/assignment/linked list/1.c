/*linked list with 3 nodes having dat 100,200,300*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
    int data;
    struct node *next;
}node_t;

node_t *head;

void init_list(void);
int is_empty(void);
node_t *create_node(int data);

void add_node_first(int data);
void add_node_last(int data);
void add_node_position(int data,int pos);

void delete_node_first(void);
void delete_node_last(void);
void delete_node_postion(void);

void display_list(void);
void free_list(void);

int main()
{
    init_list();
    add_node_first(100);
    add_node_position(200,2);
    add_node_last(300);



    display_list();
    free_list();
    
}

void init_list(void)
{
    head =NULL;
}

int is_empty(void)
{
    return head == NULL;
}

node_t *create_node(int data)
{
    node_t *newnode=(node_t*)malloc(sizeof(node_t));
    newnode->data=data;
    newnode->next=NULL;

    return newnode;
}

void add_node_first(int data)
{
    node_t *newnode=create_node(data);
    if(is_empty())
    head=newnode;
    else{
        newnode->next=head;
        head=newnode;
    }
}

void add_node_last(int data)
{
    node_t *newnode=create_node(data);

    if(is_empty())
    head=newnode;
    else
    {
        node_t *trav=head;
        while(trav->next != NULL)
        trav=trav->next;
        trav->next=newnode;
        newnode->next=NULL;
    }
}
void add_node_position(int data,int pos)
{
    node_t *newnode=create_node(data);
    if(is_empty())
    head=newnode;
    else if(pos<=1)
    {
    free(newnode);
    add_node_first(data);
    }
    else{
        node_t *trav=head;
        int i=1;
        while(i<pos-1 && trav->next != NULL)
        {
            trav=trav->next;
            i++;
        }
        newnode->next=trav->next;
        trav->next=newnode;
    }
}

void delete_node_first(void)
{
    if(is_empty())
    printf("list is empty");
    else
    {
        node_t *temp=head;
        head=head->next;
    free(temp);
    }
}

void delete_node_last(void)
{
    if(is_empty())
    printf("list is empty");
    else if(head->next == NULL)
    {
        free(head);
        head=NULL;
    }
    else{
        node_t *trav;
        for(trav=head;trav->next->next !=NULL;trav=trav->next)
        free(trav->next);
        trav->next=NULL;

    }
}

void delete_node_position(int data,int pos)
{
    if(is_empty())
    printf("list is empty");
    else if(head->next==NULL)
    {
        free(head);
        head=NULL;
    }
    else if(pos==1)
    {
        delete_node_first();
        return;
    }
    else{
        node_t *trav=head;
        for(int i=1;i<pos-1 && trav->next->next!=NULL;i++)
        trav=trav->next;

        node_t *temp=trav->next;
        trav->next=temp->next;
        free(temp);
    }
}
void display_list(void)
{
    if(is_empty())
    printf("list is empty");
    else {
        node_t *trav=head;
        printf("List :");
        while(trav != NULL)
        {
            printf("%-4d",trav->data);
            trav=trav->next;
            
        }
    }
}

void free_list(void)
{
    if(is_empty())
    printf("List is empty");
    else{
        node_t *trav=head;
        while(trav!=NULL)
        {
            node_t *temp=trav;
            trav=trav->next;
            free(temp);
        }
    }
}