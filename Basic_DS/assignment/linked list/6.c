//c programme to swap two nodes in singly list

#include<stdio.h>
#include<stdlib.h>

struct node{
    int data;
    struct node *next;
};
//void swap(int *a,int*b);

void pairswap(struct node* head)
{
    struct node* temp=head;

    while(temp != NULL && temp->next != NULL)
    {
        swap(&temp->data,&temp->next->data);

        temp=temp->next->next;
    }
}

void swap(int*a,int*b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b=temp;

}

void add(struct node** head_ref,int data)
{
    struct node* newnode=(struct node*)malloc(sizeof(struct node));

    newnode->data = data;
    newnode->next=(*head_ref);
    (*head_ref) = newnode;
}

void print_list(struct node* node)
{
    while(node != NULL)
    {
        printf("%d ",node->data);
        node=node->next;
    }
}

int main()
{
    struct node* start=NULL;

    add(&start,6);
    add(&start,5);
    add(&start,4);
    add(&start,3);
    add(&start,2);
    add(&start,1);

    printf("Linked list before swap()\n");

    print_list(start);

    pairswap(start);

    printf("\nlinked list after swapping()\n");
    print_list(start);
}