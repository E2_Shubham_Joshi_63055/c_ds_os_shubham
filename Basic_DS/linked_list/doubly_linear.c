#include<stdio.h>
#include<stdlib.h>

typedef struct node{
    char data;
    struct node *next;
    struct node *prev;
}node_t;

typedef struct list{
    node_t *head;
    node_t *tail;
}list_t;

void init_list(list_t *list);
int is_empty(list_t *list);
node_t *create_node(char data);

void display_forward(list_t *list);
void display_backward(list_t *list);
void free_list(list_t *list);

void add_node_first(list_t *list,char data);
void add_node_last(list_t *list,char data);
void add_node_position(list_t *list,char data,int pos);

void delete_node_first(list_t *list);
void delete_node_last(list_t *list);
void delete_node_position(list_t *list,int pos);

int main(void)
{
    list_t l1;
    init_list(&l1);


}

void init_list(list_t *list)
{
    list->head=list->tail=NULL;
}

int is_empty(list_t *list)
{
    return list->head==NULL && list->tail==NULL;
}

node_t *create_node(char data)
{
    node_t *newnode=(node_t*)malloc(sizeof(node_t));
    newnode->data=data;
    newnode->next=NULL;
    newnode->prev=NULL;

    return newnode;
}

void display_forward(list_t *list)
{
    //if list is empty
    if(is_empty(list))
    printf("List is empty");
    //if not empty
    else{
        printf("Forward list ::");
        node_t *trav=list->head;
        while ((trav!= NULL))
        {
            //print data of current node
            printf("%-4d",trav->data);
            // go to next node
            trav= trav->next;
            //repeat till last node
        }
        
    }
}

void display_backward(list_t *list)
{
    if(is_empty(list))
    printf("List is empty:");
    //if not empty
    else{
        printf("Backward list::");
        node_t *trav= list->tail;
        while (trav != NULL)
        {
            printf("%-4d",trav->data);

            trav= trav->prev;
        }
        
    }
}

void free_list(list_t *list)
{
    if(is_empty(list))
    printf("List is empty");
    else{
        node_t *trav =list->head;
        while(trav != NULL)
        {
            //take backup
            node_t *temp = trav;
            //next node
            trav=trav->next;
            free(temp);
            //repeat till last node
        }
    }
}

void add_node_first(list_t *list,char data)
{
    //create node
    node_t *newnode=create_node(data);
    if(is_empty(list))
        list->head=list->tail=newnode;
    //if not empty
    else
    {
        //add head into newnode -> next
        newnode->next=list->head;
        //add newnode into  head->prev
        list->head->prev=newnode;
        //add newnode into head
        list->head= newnode;
    }
}

void add_node_last(list_t *list,char data)
{
    //create node
    node_t *newnode=create_node(data);
    //if list is empty
    if(is_empty(list))
        list->head=list->tail=newnode;
    //if list is not empty
    else{
        //add tail into newnode -> prev
        newnode-> prev = list-> tail;
        //add newnode into tail-> next
        list->tail->next=newnode;
        //add newnode into tail
        list->tail=newnode;
    }
}

void delete_node_first(list_t *list)
{
    if(is_empty(list))
    printf("List is empty");
    //if has only one node
    else if (list->head->next == NULL)
    {
        free(list->head);
        list->head=list->tail=NULL;
    }
    //if has multiple node
    else
    {
        //move head to seond node
        list->head=list->head->next;
        //free memory of previoud node
        free(list->head->prev);
        //make head-> prev =null
        list->head->prev= NULL;
    }
}

void delete_node_last(list_t *list)
{
    if(is_empty(list))
    printf("List is empty");
    else if
}