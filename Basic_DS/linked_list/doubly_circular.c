#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
    char data;
    struct node *next;
    struct node *prev;
    
}
node_t ;

typedef struct list
{
    node_t *head;

}list_t;

void init_list(list_t *list);
int is_empty(list_t *list);
node_t *create_node(char data);

void display_forward(list_t *list);
void display_backward(list_t *list);
void free_list(list_t *list);

void add_node_first(list_t *list,char data);
void add_node_last(list_t *list,char data);
void add_node_position(list_t *list,char data,int pos);

void delete_node_first(list_t *list);
void delete_node_last(list_t *list);
void delete_node_position(list_t *list,int pos);

int main()
{
    list_t l1;
    init_list(&l1);
    /*
    add_node_first(&l1,'U');
    add_node_first(&l1,'S');
    add_node_first(&l1,'A');
    add_node_first(&l1,'J');
    */
    
    add_node_last(&l1,'F');
    add_node_last(&l1,'E');
    add_node_last(&l1,'D');
    add_node_last(&l1,'C');
    add_node_last(&l1,'B');
    add_node_last(&l1,'A');
    


  // delete_node_first(&l1);
  //  delete_node_last(&l1);
    //add_node_position(&l1,'N',4);

    delete_node_position(&l1,4);
    

    display_forward(&l1);
    display_backward(&l1);
    free_list(&l1);
    return 0;
}

void init_list(list_t *list)
{
    list->head = NULL;
}
int is_empty(list_t *list)
{
  return list->head==NULL;
}
node_t *create_node(char data)
{
    node_t *newnode=(node_t*)malloc(sizeof(node_t));
    newnode->data=data;
    newnode->next=NULL;
    newnode->prev=NULL;

    return newnode;
}

void display_forward(list_t *list)
{
    if(is_empty(list))
        printf("list is empty");
    else
    {
        printf("Forward list ::");
        node_t *trav= list->head;
        do
        {
            printf("%-4c",trav->data);
            trav=trav->next;
        } while (trav != list->head);
        printf("\n");
        
    }
}
void display_backward(list_t *list)
{
    if(is_empty(list))
        printf("List is empty");
    else
    {
        printf("Backard list ::");
        node_t *trav = list->head->prev;
        do
        {
            printf("%-4c",trav->data);
            trav=trav->prev;
        } while (trav != list->head->prev);
        printf("\n");
        
    }
}

void free_list(list_t *list)
{
    if(is_empty(list))
        printf("List is empty\n");
    else
    {
        node_t*trav=list->head;
        do
        {
            node_t *temp = trav;
            trav=trav->next;
            free(temp);
        } while(trav != list->head);
        list->head=NULL;
        
    }
}
void add_node_first(list_t *list,char data)
{
    //create node
    node_t *newnode= create_node(data);
    //if list is empty
    if(is_empty(list))
    {


        list->head=newnode;
        //make circular
        newnode->next=newnode;
        newnode->prev=newnode;
    }
    else //if not empty
    {
        //add head into newnode ->next
        newnode->next=list->head;
        //add last node(head->prev) into newnode->prev
        newnode->prev=list->head->prev;
        //add newnode into head ->prev
        list->head->prev=newnode;
        //add newnode into next of last node(newnode->prev->next)
        newnode->prev->next=newnode;
        //add newnode into head
        list->head=newnode;
    }

}

void add_node_last(list_t *list,char data)
{
    node_t *newnode=create_node(data);

    if(is_empty(list))
    {
        //add newnode into head
        list->head= newnode;
        newnode->next=newnode;
        newnode->prev=newnode;
    }
    //if not empty
    else
    {
        //add first node onto newnode->next
        newnode->next=list->head;
        //add last node into newnode ->prev
        newnode->prev = list->head->prev;
        //add newnode into next of last node
        list->head->prev->next=newnode;
        //add nenwode into prev of first node
        list->head->prev=newnode;
    }
}
void add_node_position(list_t *list,char data,int pos)
{
    node_t *newnode=create_node(data);
    if(is_empty(list))
    {
        list->head=newnode;
        newnode->prev=newnode;
        newnode->next=newnode;
    }
        
    else if(pos<=1)
    {
        free(newnode);
        add_node_first(list,data);
    }
    else
    {
        //traverse till pos-1
        node_t *trav=list->head;
        for(int i=1;i<pos-1 && trav->next != list->head;i++)
        trav=trav->next;

        //add pos node into newnode->next
        newnode->next=trav->next;
        //add pos -1 node into newnode ->prev
        newnode->prev=trav;
        //add newnode into prev of pos node(trav->next-<prev)
        trav->next->prev=newnode;
        //add newode into next of pos -1 node
        trav->next=newnode;
    }
}

void delete_node_first(list_t *list)
{
    if(is_empty(list))
    printf("List is empty");
    //if only node
    else if(list->head->next == list->head)
    {
        free(list->head);
        list->head=NULL;
    }
    else //if multiple node
    {
        //take backup of first node
        node_t *temp=list->head;
        //add last node into prev of second node
        list->head->next->prev=list->head->prev;
        //add second node into next of last node
        list->head->prev->next=list->head->next;
        //add second node into head
        list->head=list->head->next;
        //free temp
        free(temp);
    }
}

void delete_node_last(list_t *list)
{
    if(is_empty(list))
        printf("List is empty");
    //if only one node
    else if(list->head->next==list->head)
    {
        free(list->head);
        list->head=NULL;
    }
    //if list had multiple node
    else{
        //add seconf last node into prev of prev of first node
        list->head->prev=list->head->prev->prev;
        //free mmeory of last node
        free(list->head->prev->next);
        //add first node into next of last node
        list->head->prev->next=list->head;
    }
}

void delete_node_position(list_t *list,int pos)
{
    if(is_empty(list))
    printf("List is empty\n");
    //if list has only one node
    else if(list->head->next == list->head)
    {
        free(list->head);
        list->head=NULL;
    }
    else if ((pos <= 1))
    {
        delete_node_first(list);
    }
    else
    {
        //traverse till pos -1 node
        node_t *trav=list->head;
        for(int i=0;i<pos-1 && trav->next !=list->head;i++)
        trav=trav->next;
        if(trav->next== list->head)
        {
            delete_node_last(list);
            return;
        }
        // add pos +1 into trav->next
        trav->next= trav->next->next;
        //free memory of pos node(trav->next->prev)
        free(trav->next->prev);
        //add pos -1 node into prev of pos +1 node
        trav->next->prev=trav;
    }
    
}