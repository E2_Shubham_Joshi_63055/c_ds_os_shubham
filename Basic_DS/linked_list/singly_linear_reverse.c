#include<stdio.h>
#include<stdlib.h>

//declare type of node
typedef struct node{
    int data;
    struct node *next;
}node_t;
//declare linked list
node_t *head;

void init_list(void);
int is_empty(void);
node_t *create_node(int data);

void add_node_first(int data);
void add_node_last(int data);
void add_node_position(int data,int pos);

void delete_node_first(void);
void delete_node_last(void);
void delete_node_position(int pos);



void display_list(void);
void display_forward(node_t *trav);
void display_reverse(node_t *trav);
void reverse_list(void);
void free_list(void);

int main()
{
    init_list();
    add_node_first(10);
    add_node_first(20);
    add_node_first(30);
    add_node_first(40);

    /*
    add_node_last(50);
    add_node_last(60);


   add_node_position(70,3);
    add_node_position(80,4);

   delete_node_first();
    delete_node_last();
     delete_node_last();

     delete_node_position(3);
     delete_node_position(1);
     delete_node_position(2);
     */

    printf("forward list:");
    display_forward(head);
    printf("Reverse list:");
    display_reverse(head);

        reverse_list();
        display_list();
    free_list();

}

void init_list(void)
{
    head = NULL;

}

int is_empty(void)
{
    return head == NULL;
}
node_t *create_node(int data)
{
    node_t *newnode=(node_t *)malloc(sizeof(node_t));

    newnode->data=data;
    newnode->next=NULL;

    return newnode;
}

void add_node_first(int data)
{
    //create node with given data
    node_t *newnode=create_node(data);

    //check if list is empty
    if(is_empty())
    //add newnode into head itself
    head=newnode;
    //if list is not empty
    else{
        // add head into next of newnode
        newnode->next=head;
        //add newnode into head
        head = newnode;
    }
}

void add_node_last(int data)
{
    //craete node with given data
    node_t *newnode=create_node(data);

    //check if list is empty
    if(is_empty())
    head = newnode;
    //if list is not empty
    else{
        //start from head and travsere till last node
        node_t *trav =head;
        while(trav->next != NULL)
        trav=trav->next;
        //add newnode into next of last node(trav->next)
        trav->next = newnode;
        //add NULL into next of newnode
        newnode->next=NULL;
    }
}
void add_node_position(int data,int pos)
{
    // carete node
    node_t *newnode = create_node(data);

    //check if empty
    if(is_empty())
    head = newnode;

    //if pis is less than or equal to 1
    else if(pos<=1)
    {
        free(newnode);
        add_node_first(data);
    }
    // if list is not empty
    else
    {
        // start head and traverse till pos-1 node
        node_t *trav = head;
        int i =1;
        //if pos is greater thanm expected then add at last
        while (i<pos-1  && trav->next !=NULL)
        {
            trav=trav->next;
            i++;
        }
           //add address of pos node into next of newnode
    newnode->next=trav->next;
    //add newnode into next of pos -1 node
    trav->next=newnode;
    }
 
}
void delete_node_first(void)
{
    //check list is empty
    if(is_empty())
    printf("Lists is empty");
    //if list not empty
    else
    {
        //take backup of first node into temp
        node_t *temp=head;
        //add second node into head
        head= head->next;
        //free memory of backuped node
        free(temp);
    }
}
void delete_node_last(void)
{
    //check if list is empty
    if(is_empty())
    printf("list is empty");
    //if list has only one node
    else if(head->next == NULL)
    {
        //free memory of singlr node
        free(head);
        //head=NULL;
        head = NULL;
 
    }
    else
    {
        //traverse till second last node
        node_t *trav;
        for(trav=head;trav->next->next != NULL;trav=trav->next) ;
        free(trav->next);
        trav->next=NULL;
    }


}

void delete_node_position(int pos)
{
    if(is_empty())
    printf("list is empty");
    //if only one node
    else if(head->next==NULL)
    {
        //free head
        free(head);
        head=NULL;
    }
    //if pos is less than equal to 1
    else if(pos==1)
    {
        delete_node_first();
        return;
    }
    //if multiple node
    else{
        //traverse till pos-1 node
        node_t *trav=head;
        for(int i=1;i<pos-1 && trav->next->next!= NULL;i++)
        trav=trav->next;

        //take backup of pos into temp
        node_t *temp = trav->next;
        //add pos+1 node into pos-1 node
        trav->next=temp->next;
        //free temp
        free(temp);
    }
    
}

void display_list(void)
{
    //check if empty
    if(is_empty())
    printf("list is empty");
    //if not empty
    else{
        //staart trav at head
      node_t *trav=head;
      printf("List:");
      while(trav != NULL)
      {
        //printf(visit) data of current node
        printf("%-4d",trav->data);
        //go on next node
        trav=trav->next;
        //repart till last node
      }  
    }
}

void free_list(void)
{
    if(is_empty())
    printf("List is empty");
    else{
        //start trav at head
        node_t *trav =head;
        while(trav!=NULL)
        {
            //TAKE BACKUP OF NURRENT NODE TEMP
                    node_t *temp=trav;
                    //go next node
                    trav=trav->next;
                    //free temp
                    free(temp);
                    //repeat till last node
        }

    }
}

void display_forward(node_t *trav)
{
    //stop if trav is NULL
    if(trav==NULL)
    return;
    //print data of current node
    printf("%-4d",trav->data);
    //go next node
    display_forward(trav->next);
}

void display_reverse(node_t *trav)
{
    //stop if trav is NULL
    if(trav == NULL)
    return ;
    display_reverse(trav->next);
    printf("%-4d",trav->data);
}

void reverse_list(void)
{
    //start t1 at first node and t2 at second node
    node_t *t1=head , *t2=head->next;

    //make t1>next NULL
    t1->next =NULL;
    while(t2 != NULL)
    {
        //take backup of next node of t2
        node_t *t3=t2->next;
        //reverse link between t1 and t2
        t2->next = t1;
        //increment t1 and t2
        t1 = t2;
        t2 = t3;
        //repeat till t2 != NULL
    }
    //modify head
    head=t1;

}