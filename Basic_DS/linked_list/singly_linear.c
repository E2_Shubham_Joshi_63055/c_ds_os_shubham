#include<stdio.h>
#include<stdlib.h>

//declare type of node
typedef struct node{
    int data;
    struct node *next;
}node_t;
//declare linked list
node_t *head;

void init_list(void);
int is_empty(void);
node_t *create_node(int data);

void add_node_first(int data);
void add_node_last(int data);
void add_node_position(int data,int pos);

void delete_node_first(int data);
void delete_node_last(int data);
void delete_node_position(int data,int pos);

void display_list(void);
void free_list(void);

int main()
{
    init_list();
    add_node_first(10);
    add_node_first(20);
    add_node_first(30);
    add_node_first(40);

    add_node_last(50);


    display_list();
    free_list();

}

void init_list(void)
{
    head = NULL;

}

int is_empty(void)
{
    return head == NULL;
}
node_t *create_node(int data)
{
    node_t *newnode=(node_t *)malloc(sizeof(node_t));

    newnode->data=data;
    newnode->next=NULL;

    return newnode;
}

void add_node_first(int data)
{
    //create node with given data
    node_t *newnode=create_node(data);

    //check if list is empty
    if(is_empty())
    //add newnode into head itself
    head=newnode;
    //if list is not empty
    else{
        // add head into next of newnode
        newnode->next=head;
        //add newnode into head
        head = newnode;
    }
}

void add_node_last(int data)
{
    //craete node with given data
    node_t *newnode=create_node(data);

    //check if list is empty
    if(is_empty())
    head = newnode;
    //if list is not empty
    else{
        //start from head and travsere till last node
        node_t *trav =head;
        while(trav->next !=NULL)
        trav=trav->next;
        //add newnode into next of last node(trav->next)
        trav->next = newnode;
        //add NULL into next of newnode
        newnode->next=NULL;
    }
}
void add_node_position(int data,int pos)
{
    // carete node
    node_t *newnode = create_node(data);

    //check if empty
    if(is_empty())
    head = newnode;

    //if pis is less than or equal to 1
    else if(pos<=1)
    {
        free(newnode);
        add_node_first(data);
    }
}
