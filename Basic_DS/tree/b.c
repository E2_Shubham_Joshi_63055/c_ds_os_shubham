#include<stdio.h>
#include<stdlib.h>

//declare node
typedef struct node{
    int data;
    struct node *left;
    struct node *right;
}node_t;

typedef struct bst{
    node_t *root;
}bst_t;

void init_bst(bst_t *bst);
int is_bst_empty(bst_t *bst);
node_t *create_node(int data);

void add_node_bst(bst_t *bst,int data);
void free_all_bst(bst_t *bst);
void free_bst(node_t *trav);

void preorder(node_t *trav);
void inorder(node_t *trav);
void postorder(node_t *trav);

int main()
{
    bst_t t1;

    init_bst(&t1);

    add_node_bst(&t1,50);
    add_node_bst(&t1,30);
    add_node_bst(&t1,60);
    add_node_bst(&t1,40);


    printf("Preorder :: ");
    preorder(t1.root);
    printf("\n");

    printf("Inorder :: ");
    inorder(t1.root);
    printf("\n");

    printf("Postorder :: ");
    postorder(t1.root);
    printf("\n");

    free_all_bst(&t1);
    return 0;
}

void init_bst(bst_t *bst)
{
    bst->root=NULL;
}
int is_bst_empty(bst_t *bst)
{
    return bst->root==NULL;
}

node_t *create_node(int data)
{
    node_t *newnode=(node_t*)malloc(sizeof(node_t));

    newnode->data=data;
    newnode->left=NULL;
    newnode->right=NULL;

    return newnode;
}

void add_node_bst(bst_t *bst,int data)
{
    //create node
    node_t *newnode=create_node(data);
    //check if bst is empty
    if(is_bst_empty(bst))
        //add newmode into root
        bst->root=newnode;
    //if bst is not empty
    else
    {
        node_t *trav = bst->root;
        while(1)
        {
            //copmare data of current node into newnode
            if(trav->data > data)
            {
                //add node into left side
                //check if left of current is NULL(empty)
                if(trav->left == NULL)
                {
                    //addd newnode into left of current node
                    trav->left =newnode;
                    break;
                }
                //go on left node
                trav=trav->left;
            }
            else
            {
                //add node into right side
                //check id current node is empty(NULL)
                if(trav->right == NULL)
                {
                    trav->right=newnode;
                    break;
                }
                //go on right
                trav=trav->right;
            }
        }
    }
}

void preorder(node_t *trav)
{
    if(trav == NULL)
    return;
    printf("%-4d",trav->data);
    preorder(trav->left);
    preorder(trav->right);
}

void inorder(node_t *trav)
{
    if(trav == NULL)
    return;
    inorder(trav->left);
    printf("%-4d",trav->data);
    inorder(trav->right);
}

void postorder(node_t *trav)
{
    if(trav == NULL)
    return;
    postorder(trav->left);
    postorder(trav->right);
    printf("%-4d",trav->data);
}

void free_all_bst(bst_t *bst)
{
    free_bst(bst->root);
    bst->root = NULL;

}
void free_bst(node_t *trav)
{
    if(trav == NULL)
    return;
    free_bst(trav->left);
    free_bst(trav->right);
    free(trav);
}