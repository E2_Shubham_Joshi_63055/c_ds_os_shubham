#include<stdio.h>

#define N 9

int binary_search(int *arr,int n,int key);

int main()

{
    int arr[N]={11,22,33,44,55,66,77,88,99};
    int key,ret;
    printf("Enter key :");
    scanf("%d",&key);

    ret=binary_search(arr,N,key);

    if(ret == -1)
    {
        printf("Key is not found.");
    }
    else
    printf("Key is found at index%d\n",ret);
}

int binary_search(int *arr,int n,int key)
{
    int left=0,right=n-1,mid;
    while(left <= right)
    {
    mid=(left+right)/2;
    if(arr[mid]==key)
        return mid;
    else if(key<arr[mid])
        right=mid-1;
    else 
        left=mid+1;
    }
    return -1;
}