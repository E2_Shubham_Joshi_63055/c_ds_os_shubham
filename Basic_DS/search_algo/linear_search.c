#include<stdio.h>

#define SIZE 9

typedef enum boolean{
    FALSE,TRUE
}bool_t;

bool_t linear_search(int *arr,int size,int key);

int main()
{
    int arr[SIZE]={10,20,30,40,50,60,70,80,90};
    int key;
    bool_t ret;
    printf("Enter key to be searched :");
    scanf("%d",&key);

    ret=linear_search(arr,SIZE,key);
    if(ret==TRUE)
    printf("Key is found\n");
    else
    printf("Key is not found\n");

    return 0;

}

bool_t linear_search(int *arr,int size,int key)
{
    //traversef from start to end
    for(int i=0;i<SIZE;i++)
    {
        if(arr[i]==key)
        return TRUE;
    }
    return FALSE;
}