#include<stdio.h>

int sum(int,int); //function declaration

int main()
{
    int s=0;
    int (*ptr)(int,int)=sum;  // function pointer initilization
    s=ptr(5,4);                  //function pointer declaration // function calling

    printf("sum=%d\n",s);
    
}
int sum(int a,int b)            //function defination
{
    return a+b;
}
